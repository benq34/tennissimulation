using System.Collections.Generic;

namespace TennisSimulation
{
    public class OutputModel
    {
        public List<ResultModel> results { get; set; }
    }
}

