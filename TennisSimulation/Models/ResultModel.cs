namespace TennisSimulation
{
    public class ResultModel
    {
        public int order{ get; set; }
        public int player_id{ get; set; }
        public int gained_experience{ get; set; }
        public int total_experience{ get; set; }
        
        
    }
}