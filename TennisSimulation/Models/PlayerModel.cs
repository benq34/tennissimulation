
namespace TennisSimulation
{
    public class PlayerModel
    {
        public int id;
        public Hand hand;
        public SkillModel skills;
        public int experience;

        public int gained_experience = 0;
        public int first_experience = 0;

    }
}