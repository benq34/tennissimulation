namespace TennisSimulation
{
    public class SkillModel
    {
        public int clay{ get; set; }
        public int grass{ get; set; }
        public int hard{ get; set; }
    }
}