namespace TennisSimulation
{
    public class MatchResult
    {
       public PlayerModel winner;
       public PlayerModel loser;
    }
}