using System;
using System.Collections.Generic;

namespace TennisSimulation.Processors
{
    public class GameProcessor
    {
        private TournamentModel tournament;

        private PlayerModel[] players = new PlayerModel[2];
        private int[] points = new int[] {0, 0};

        public GameProcessor(PlayerModel player1, PlayerModel player2, TournamentModel tournament)
        {
            this.tournament = tournament;
            players[0] = player1;
            players[1] = player2;
        }
       
        public MatchResult Play()
        {
            //Executing all rules
            ExecuteMatchRule();
            ExecuteHandRule();
            ExecuteExperienceRule();
            ExecuteSurfaceRule();
            
            int winner = CalculateResult();
            
           //Scoreless check
            if (winner < 0 || winner >= players.Length)
                return null;
             
            int loser = winner == 0 ? 1 : 0;

            var matchResult = new MatchResult
            {
                winner = players[winner],
                loser = players[loser]
            };
            
            AddExperience(matchResult.winner,true);
            AddExperience(matchResult.loser,false);
            
            return matchResult;
        }

        //Adding match point to all players default
        private void ExecuteMatchRule()
        {
            for (int i = 0; i < players.Length; i++)
            {
                addPoint(i, (int) Points.MatchPoint);
            }
        }

        //Adding left hand point to players who use left hand
        private void ExecuteHandRule()
        {
            for (int i = 0; i < players.Length; i++)
            {
                if (players[i].hand == Hand.Left)
                {
                    addPoint(i, (int) Points.LeftHandPoint);
                }
            }
        }

        //Adding experience point to players who has higher experience
        private void ExecuteExperienceRule()
        {
            if (players[0].experience != players[1].experience)
            {
                int moreExperienced = players[0].experience > players[1].experience ? 0 : 1;
                addPoint(moreExperienced, (int) Points.ExperiencePoint);
            }
        }

        //Adding surface point to players who has powerful surface skill
        private void ExecuteSurfaceRule()
        {
            string surface = tournament.surface.ToString();
            int player1_surfaceValue = (int)players[0].skills.GetType().GetProperty(surface).GetValue(players[0].skills,null);
            int player2_surfaceValue = (int)players[1].skills.GetType().GetProperty(surface).GetValue(players[1].skills,null);

           if (player1_surfaceValue != player2_surfaceValue)
           {
               int moreExperienced = player1_surfaceValue > player2_surfaceValue ? 0 : 1;
               addPoint(moreExperienced, (int) Points.SurfacePoint);
           }
        }

        //Updating points array
        private void addPoint(int side, int amount)
        {
            points[side] += amount;
        }

        //Calculating result
        private int CalculateResult()
        {
            int winner;
            
            int total = points[0] + points[1];
            float result = (float)points[0] / (float)total;
            
            if (result < 0.5f)
                winner = 1;
            else if (result > 0.5f)
                winner = 0;
            else
                winner = -1;//scoreless
        
            return winner;
        }
        
        //Adding experience depends on tournament type
        public void AddExperience(PlayerModel player, bool win)
        {
            if (tournament.type == TournamentType.League)
            {
                 int result =  win ? (int) Points.LeagueWinExperiencePoint : (int) Points.LeagueLoseExperiencePoint;
                 player.experience += result;
                 player.gained_experience += result;
            }
            else
            {
                int result =  win ? (int) Points.EliminationWinExperiencePoint : (int) Points.EliminationLoseExperiencePoint;
                player.experience += result;
                player.gained_experience += result;
            }
        }
       

    }
}