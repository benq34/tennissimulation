using System;
using System.Collections.Generic;
using TennisSimulation.Processors;

namespace TennisSimulation
{
    public class MainProcessor
    {
        private InputModel inputModel;
        
        
        public MainProcessor(InputModel inputModel)
        {
            this.inputModel = inputModel;
        }
       
        public OutputModel Start()
        {
            //Set experiences before the tournaments
            setFirstExperience();
           
            //Plays tournaments
            for (int i = 0; i < inputModel.tournaments.Count; i++)
            {
                TournamentModel tournament = inputModel.tournaments[i];
                Console.WriteLine("--------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine("{0} {1} {2}",tournament.id+". Turnuva başlıyor.", tournament.surface , tournament.type);
                TournamentProcessor tournamentProcessor=new TournamentProcessor(tournament,inputModel.players);
                tournamentProcessor.Start();
            }
            
            //Sorting after tournaments
            SortPlayers(inputModel.players);
            
            //Importing users(players => outputModel)
            OutputModel outputModel = new OutputModel();
            outputModel.results=new List<ResultModel>();
            for (int i = 0; i < inputModel.players.Count; i++)
            {
                ResultModel result = new ResultModel();
                result.order = i+1;
                result.player_id = inputModel.players[i].id;
                result.gained_experience = inputModel.players[i].gained_experience;
                result.total_experience = inputModel.players[i].experience;
                
                outputModel.results.Add(result);
            }
            
            //Printing total winner
            Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("Tournaments are over");           
            Console.WriteLine("{0} {1}","Total winner id: "+outputModel.results[0].player_id, "Total Remaining: "+outputModel.results.Count);


            return outputModel;
            
        }
        
        //Sorting function
        public void SortPlayers(List<PlayerModel> players)
        {
            players.Sort((x, y) => {
                var ret = y.gained_experience.CompareTo(x.gained_experience);
                if (ret == 0) ret = y.first_experience.CompareTo(x.first_experience);
                return ret;
            });

        }

        //Set experiences for the sorting
        public void setFirstExperience()
        {
            for (int i = 0; i < inputModel.players.Count; i++)
            {
                inputModel.players[i].first_experience = inputModel.players[i].experience;
            }
        }
        
        
    }
}