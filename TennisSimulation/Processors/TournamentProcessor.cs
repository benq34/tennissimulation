using System;
using System.Collections.Generic;
using System.Linq;

namespace TennisSimulation.Processors
{
    public class TournamentProcessor
    {
        private TournamentModel tournament;
        private List<PlayerModel> players;
        private List<PlayerModel> queue;

        public TournamentProcessor(TournamentModel tournament, List<PlayerModel> players)
        {
            this.tournament = tournament;
            this.players = players;
        }

        public void Start()
        {
            //Execute function by tournament type
            switch (tournament.type)
            {
                case TournamentType.League:
                    ExecuteLeague();
                    break;
                case TournamentType.Elimination:
                    ExecuteElimination();
                    break;
                // TODO: New tournament types can be added.
            }
        }

        public void ExecuteElimination()
        {
            queue = new List<PlayerModel>(players);
            
           //Matchmaking and play matches
            while (queue.Count != 1)
            {
                PlayerModel player1 = Pop();
                PlayerModel player2 = Pop();
                
                GameProcessor game =  new GameProcessor(player1,player2,tournament);
                MatchResult matchResult = game.Play();

                //Scoreless check
                if (matchResult == null)
                {
                    Console.WriteLine("{0}","Scoreless");
                    break;
                }
                
                updatePlayer(matchResult.winner);
                updatePlayer(matchResult.loser);
             
                Push(matchResult.winner);
            }
            //Printing tournament winner
            Console.WriteLine("{0} {1}","Tournament winner id: "+queue[0].id, "Total Remaining: "+queue.Count);
        
        }

        public void ExecuteLeague()
        {
            queue = new List<PlayerModel>(players);
           
            //Matchmaking and play matches
            for (int i = 0; i < queue.Count; i++)
            {
                for (int j = 0; j < queue.Count; j++)
                {
                    if(i != j)
                    {
                        PlayerModel player1 = queue[i];
                        PlayerModel player2 = queue[j];
                      
                        GameProcessor game =  new GameProcessor(player1,player2,tournament);
                        MatchResult matchResult = game.Play();
                        
                        //Scoreless check
                        if(matchResult == null)
                            break;
                        
                        updatePlayer(matchResult.winner);
                        updatePlayer(matchResult.loser);
                        
                    }
                }
            }
            
            //Printing tournament winner
            Console.WriteLine("{0} {1}","Tournament winner id: "+GetWinnerPlayer(queue).id, "Total Remaining: "+queue.Count);

        }

        //Removing match player from queue for the elimination type
        private PlayerModel Pop()
        {
            PlayerModel player = queue[0];
            queue.RemoveAt(0);

            return player;
        }

        //Adding winning player to queue for the elimination type
        private void Push(PlayerModel winner)
        {
            queue.Add(winner);
        }
        
        //Updating playerlist after match result
        public void updatePlayer(PlayerModel updatedPlayer)
        {
            var player = players.Where(players => players.id == updatedPlayer.id).FirstOrDefault();
            if (player != null)
            {
                player = updatedPlayer;
            }
        }
         
        //Getting winner function
        public PlayerModel GetWinnerPlayer(List<PlayerModel> players)
        {
            players.Sort((x, y) => {
                var ret = y.gained_experience.CompareTo(x.gained_experience);
                if (ret == 0) ret = y.first_experience.CompareTo(x.first_experience);
                return ret;
            });

            return players[0];

        }
    }
}