﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace TennisSimulation
{
    class Program
    {
        public static void Main(string[] args)
        {
            InputModel inputModel;
            OutputModel outputModel;
            
            //Import json file in working directory(input.json)
            using (StreamReader r = new StreamReader(@"input.json"))
            {
                string json = r.ReadToEnd();
                inputModel = JsonConvert.DeserializeObject<InputModel>(json);
            }
            
            //Plays tournaments
            MainProcessor manager=new MainProcessor(inputModel);
            outputModel = manager.Start();
          
            
           //Export json file in working directory(output.json)
           using (StreamWriter file = File.CreateText(@"output.json"))
           {
               JsonSerializer serializer = new JsonSerializer();
             
               serializer.Serialize(file, outputModel);
           }
           
           Console.WriteLine("Output model is exported as output.json file(Working Directory)");     
           
        }

    }
   
}