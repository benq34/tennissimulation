namespace TennisSimulation
{
    public enum Surface {
        clay,
        grass,
        hard
    };
    public enum TournamentType {
        Elimination,
        League
    };

    public enum Hand
    {
        Left,
        Right
    }

    public enum Points
    {
        MatchPoint = 1,
        LeftHandPoint = 2,
        ExperiencePoint = 3,
        SurfacePoint = 4,
        EliminationWinExperiencePoint=20,
        EliminationLoseExperiencePoint=10,
        LeagueWinExperiencePoint=10,
        LeagueLoseExperiencePoint=1
    }
}